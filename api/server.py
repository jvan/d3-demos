#!/usr/bin/env python2

from flask import Flask, jsonify
from flask.ext.cors import CORS

import os
from os import path

import pygments
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

lexers = { 
    "coffee": get_lexer_by_name("python", stripall=True),
    "css":    get_lexer_by_name("css", stripall=True)
}

formatter = HtmlFormatter()

def highlight(code, lang):
    return pygments.highlight(code, lexers[lang], formatter)

app = Flask(__name__)

CORS(app)

host = 'localhost'

@app.route('/api')
def ping():
    return jsonify({"status": "ok"})

@app.route('/api/demos/<category>')
def get_all_demos(category):
    demo_root = '../app/pods/components/{}'.format(category)

    demos = sorted(os.listdir(demo_root))
    return jsonify({"demos": demos})

@app.route('/api/source/<category>/<demo>')
def get_source(category, demo):
    demo_dir = '../app/pods/components/{}/{}/'.format(category, demo)
    
    coffee = open(path.join(demo_dir, 'component.coffee')).read()
    css = open(path.join(demo_dir, 'styles.css')).read()

    return jsonify({"source": { 
        "coffee": highlight(coffee, "coffee"), 
        "css":    highlight(css, "css")} 
    })


if __name__ == "__main__":
    app.run(host=host, debug=True)
