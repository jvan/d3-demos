`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

ChartRoute = Ember.Route.extend

   model: (params) ->
      @set 'title', params.chart_name

      labels = 'alfa bravo charlie delta echo foxtrot golf hotel india juliett
      kilo lima mike november oscar papa quebec romeo sierra tango uniform
      victor whisky x-ray yankee zulu'.w()

      randomValue = (min, max) ->
         min + (max-min)*Math.random()

      for label in labels
         label: label
         value: randomValue(2.0, 10.0)

   setupController: (controller, model) ->
      @_super arguments...

      controller.set 'title', @get('title')

      url = 'https://bitbucket.org/jvan/d3-demos/raw/HEAD/app/pods/components/charts'

      getCoffee = $.ajax
         url: "#{url}/#{@get 'title'}/component.coffee"
         dataType: 'text'

      getCss = $.ajax
         url: "#{url}/#{@get 'title'}/styles.css"
         dataType: 'text'

      promises =
        coffee: getCoffee
        css:    getCss

      Ember.RSVP.hash(promises).then (hash) ->
         controller.set 'model.sourceCode',
          coffee: hash.coffee
          css:    hash.css

`export default ChartRoute`
