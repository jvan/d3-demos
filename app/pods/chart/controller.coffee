`import Ember from 'ember'`

ChartController = Ember.Controller.extend
   title: null

   chartComponent: Em.computed 'model.title', ->
      "charts/#{@get 'title'}"

`export default ChartController`
