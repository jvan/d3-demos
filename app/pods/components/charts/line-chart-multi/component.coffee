`import Ember from 'ember'`

ChartsLineChartMultiComponent = Ember.Component.extend
   attributeBindings: 'width height data'.w()

   didInsertElement: ->
      width  = @get 'width'
      height = @get 'height'

      margin = top: 40, right: 20, bottom: 60, left: 40

      lines = []
      for i in [0...8]
         line = for data in @get('data')
            label: data.label
            value: data.value += -3.0 + 6.0 * Math.random()

         lines.push line

      mins = lines.map (line) -> d3.min(line, (d) -> d.value)
      maxs = lines.map (line) -> d3.max(line, (d) -> d.value)

      console.log mins
      console.log maxs

      #console.log lines
      console.log lines[0]

      svg = d3.select("#chart")
         .attr("width", width)
         .attr("height", height)
       .append("g")
         .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

      width = width - margin.left - margin.right
      height = height - margin.top - margin.bottom

      color = d3.scale.category20c()

      formatPercent = d3.format(".0")

      x = d3.scale.ordinal()
         .rangeRoundBands([0, width], 0.15)

      y = d3.scale.linear()
         .range([height, 0])

      line = d3.svg.line()
         .x((d) -> x(d.label))
         .y((d) -> y(d.value))
         .interpolate("basis")

      x.domain(@get('data').map((d) -> d.label))
      #y.domain([0, 12])
      y.domain [d3.min(mins), d3.max(maxs)]
   

      xAxis = d3.svg.axis()
         .scale(x)
         .orient("bottom")

      yAxis = d3.svg.axis()
         .scale(y)
         .orient("left")
         .tickFormat(formatPercent)

      svg.append("g")
         .attr("class", "x axis")
         .attr("transform", "translate(0," + height + ")")
         .call(xAxis)
       .selectAll("text")
         .style("text-anchor", "end")
         .attr("dx", "-0.8em")
         .attr("dy", "0.15em")
         .attr("transform", "rotate(-65)")

      svg.append("g")
         .attr("class", "y axis")
         .call(yAxis)
       .append("text")
         .attr("transform", "rotate(-90)")
         .attr("y", 6)
         .attr("dy", ".71em")
         .style("text-anchor", "end")
         .text("value")

      for i in [0...8]
         svg.append("path")
            .attr("class", "line")
            .attr("d", line(lines[i]))
            .style("stroke", (d,j) -> color(i))

`export default ChartsLineChartMultiComponent`
