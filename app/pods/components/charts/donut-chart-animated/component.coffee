`import Ember from 'ember'`

midAngle = (d) ->
   d.startAngle + (d.endAngle - d.startAngle)/2

ChartsDonutChartLabelsComponent = Ember.Component.extend
   attributeBindings: 'width height data'.w()

   actions:
      randomize: ->
         @get('data').forEach (item) ->
            value = Ember.get item, 'value'
            Ember.set item, 'value', (2.0 + 8.0 * Math.random())

   didInsertElement: ->
      width  = @get 'width'
      height = @get 'height'
      @radius = Math.min(width, height) / 2

      @svg = d3.select("#chart")
         .attr("width", width)
         .attr("height", height)
         .append("g")
         .attr("transform", "translate(#{width/2}, #{height/2})")

      @svg.append("g")
         .attr("class", "slices")
      @svg.append("g")
         .attr("class", "labels")
      @svg.append("g")
         .attr("class", "lines")

      @pie = d3.layout.pie()
         .sort(null)
         .value((d) -> d.value)

      @arc = d3.svg.arc()
         .outerRadius(@radius * 0.8)
         .innerRadius(@radius * 0.4)

      @outerArc = d3.svg.arc()
         .innerRadius(@radius * 0.9)
         .outerRadius(@radius * 0.9)

      @draw()

   drawSlices: ->
      color = d3.scale.category20b()

      slice = @svg.select(".slices").selectAll("path.slice")
         .data(@pie(@get 'data'))

      slice.enter()
         .insert("path")
         .style("fill", (d, i) -> color(i) )
         .attr("class", "slice")

      arc = @get 'arc'

      slice
         .transition().duration(1000)
         .attrTween("d", (d) ->
            this._current =this._current or d
            interpolate = d3.interpolate(this._current, d)
            this._current = interpolate(0)
            (t) ->
               arc(interpolate(t))
         )

      slice.exit()
         .remove()

   drawText: ->
      text = @svg.select(".labels").selectAll("text")
         .data(@pie(@get 'data'))

      text.enter()
         .append("text")
         .attr("dy", ".35em")
         .text((d) -> d.data.label)

      radius   = @get 'radius'
      arc      = @get 'arc'
      outerArc = @get 'outerArc'

      text.transition().duration(1000)
         .attrTween("transform", (d) ->
            this._current = this._current or d
            interpolate = d3.interpolate(this._current, d)
            this._current = interpolate(0)
            (t) =>
               d2 = interpolate(t)
               pos = outerArc.centroid(d2)
               pos[0] = radius * (if midAngle(d2) < Math.PI then 1 else -1)
               "translate(#{pos})"
         )
         .styleTween("text-anchor", (d) ->
            this._current = this._current or d
            interpolate = d3.interpolate(this._current, d)
            this._current = interpolate(0)
            (t) ->
               d2 = interpolate(t)
               if midAngle(d2) < Math.PI then "start" else "end"
         )

      text.exit()
         .remove()

   drawPolylines: ->
      polyline = @svg.select(".lines").selectAll("polyline")
         .data(@pie(@get 'data'))
      
      polyline.enter()
         .append("polyline")

      radius   = @get 'radius'
      arc      = @get 'arc'
      outerArc = @get 'outerArc'

      polyline.transition().duration(1000)
         .attrTween("points", (d) ->
            this._current = this._current or d
            interpolate = d3.interpolate(this._current, d)
            this._current = interpolate(0)
            (t) ->
               d2 = interpolate(t)
               pos = outerArc.centroid(d2)
               pos[0] = radius * 0.95 * (if midAngle(d2) < Math.PI then 1 else -1)
               [arc.centroid(d2), outerArc.centroid(d2), pos]
         )

      polyline.exit()
         .remove()

   draw: Em.observer 'data.@each.value', ->
      @drawSlices()
      @drawText()
      @drawPolylines()

`export default ChartsDonutChartLabelsComponent`
