`import Ember from 'ember'`

ChartsVoronoiTopologyComponent = Ember.Component.extend
   
   didInsertElement: ->
      width  = 960
      height = 500

      points = for i in [0...100]
         [Math.random() * width, Math.random() * height]

      console.log points

      voronoi = d3.geom.voronoi()
         .clipExtent([[-1, -1], [width + 1, height + 1]])

      console.log voronoi(points)
      
      color = d3.scale.category10()

      svg = d3.select("#chart")
         .attr("width", width)
         .attr("height", height)

      svg.selectAll("path")
            .data(voronoi(points))
         .enter().append("path")
            .attr("class", "cell")
            .attr("d", (d) -> "M#{d.join("L")}Z")
            .style("fill", (d,i) -> color(i))
            .style("opacity", 0.7)

      svg.selectAll("circle")
            .data(points)
         .enter().append("circle")
            .style("fill", (d,i) -> color(i))
            .attr("transform", (d) -> "translate(#{d})")
            .attr("r", 2.5)

`export default ChartsVoronoiTopologyComponent`
