`import Ember from 'ember'`

ChartsBarChartComponent = Ember.Component.extend
   attributeBindings: 'width height data'.w()

   didInsertElement: ->
      width  = @get 'width'
      height = @get 'height'

      margin = top: 40, right: 20, bottom: 60, left: 40

      svg = d3.select("#chart")
         .attr("width", width)
         .attr("height", height)
       .append("g")
         .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

      width = width - margin.left - margin.right
      height = height - margin.top - margin.bottom

      formatPercent = d3.format(".0")

      x = d3.scale.ordinal()
         .rangeRoundBands([0, width], 0.15)

      y = d3.scale.linear()
         .range([height, 0])

      x.domain(@get('data').map((d) -> d.label))
      y.domain([0, 12])

      xAxis = d3.svg.axis()
         .scale(x)
         .orient("bottom")

      yAxis = d3.svg.axis()
         .scale(y)
         .orient("left")
         .tickFormat(formatPercent)

      svg.append("g")
         .attr("class", "x axis")
         .attr("transform", "translate(0," + height + ")")
         .call(xAxis)
       .selectAll("text")
         .style("text-anchor", "end")
         .attr("dx", "-0.8em")
         .attr("dy", "0.15em")
         .attr("transform", "rotate(-65)")

      svg.append("g")
         .attr("class", "y axis")
         .call(yAxis)
       .append("text")
         .attr("transform", "rotate(-90)")
         .attr("y", 6)
         .attr("dy", ".71em")
         .style("text-anchor", "end")
         .text("value")

      svg.selectAll(".bar")
         .data(@get 'data')
       .enter().append("rect")
         .attr("class", "bar")
         .attr("x", (d) -> x(d.label))
         .attr("width", x.rangeBand())
         .attr("y", (d) -> y(d.value))
         .attr("height", (d) -> height - y(d.value))

`export default ChartsBarChartComponent`
