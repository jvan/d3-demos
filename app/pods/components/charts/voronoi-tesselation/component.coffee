`import Ember from 'ember'`

ChartsVoronoiTesselationComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      @vertices = d3.range(100).map (d) ->
         [ Math.random() * width, Math.random() * height ]

      @voronoi = d3.geom.voronoi()
         .clipExtent([[0, 0], [width, height]])

      self = this

      svg = d3.select("#chart")
         .attr("width", width)
         .attr("height", height)
         .on("mousemove", -> self.vertices[0] = d3.mouse(this); self.redraw())

      @path = svg.append("g").selectAll("path")

      svg.selectAll("circle")
            .data(@vertices.slice(1))
         .enter().append("circle")
            .attr("transform", (d) -> "translate(#{d})")
            .attr("r", 1.5)

      @redraw()

   redraw: ->
      @path = @path
         .data(@voronoi(@vertices), @polygon)

      @path.exit().remove()

      @path.enter().append("path")
         .attr("class", (d,i) -> "q" + (i % 9) + "-9")
         .attr("d", @polygon)

      @path.order()

   polygon: (d) ->
      "M#{d.join('L')}Z"

`export default ChartsVoronoiTesselationComponent`
