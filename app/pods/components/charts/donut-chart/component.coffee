`import Ember from 'ember'`

ChartsDonutChartComponent = Ember.Component.extend
   attributeBindings: 'width height data'.w()

   didInsertElement: ->
      width  = @get 'width'
      height = @get 'height'
      radius = Math.min(width, height) / 2

      pie = d3.layout.pie()
         .sort(null)
         .value((d) -> d.value)

      arc = d3.svg.arc()
         .innerRadius(radius * 0.4)
         .outerRadius(radius * 0.8)

      color = d3.scale.category20b()

      svg = d3.select("#chart")
            .attr("width", width)
            .attr("height", height)
         .append("g")
            .attr("transform", "translate(#{width/2}, #{height/2})")

      data = @get 'data'

      svg.selectAll("path")
            .data(pie(data))
         .enter().insert("path")
            .style("fill", (d, i) -> color(i))
            .attr("d", (d) -> arc(d))

`export default ChartsDonutChartComponent`
