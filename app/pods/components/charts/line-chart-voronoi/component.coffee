`import Ember from 'ember'`

ChartsLineChartVoronoiComponent = Ember.Component.extend
   attributeBindings: 'width height data'.w()
   showVoronoi: false

   toggleVoronoiGrid: Em.observer 'showVoronoi', ->
      @voronoiGroup.classed("voronoi--show", @get('showVoronoi'))

   actions:
      toggleVoronoi: ->
         @toggleProperty 'showVoronoi'

   didInsertElement: ->
      width  = @get 'width'
      height = @get 'height'

      margin = top: 40, right: 20, bottom: 60, left: 40

      lines = []
      for data in @get('data')
         line =
            label: data.label
            values: null

         line.values = for i in [0...100]
                     line: line
                     index: i
                     value: data.value += -1.0 + 2.0 * Math.random()
         #label: data.label
         #value: data.value += -3.0 + 6.0 * Math.random()

         lines.push line

      console.log "LINES ( BEGIN )"
      console.log lines
      console.log "LINES ( END )"

      mins = lines.map (line) -> d3.min(line.values, (d) -> d.value)
      maxs = lines.map (line) -> d3.max(line.values, (d) -> d.value)

      console.log mins
      console.log maxs

      svg = d3.select("#chart")
         .attr("width", width)
         .attr("height", height)
       .append("g")
         .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

      width = width - margin.left - margin.right
      height = height - margin.top - margin.bottom

      color = d3.scale.category20c()

      voronoi = d3.geom.voronoi()
         .x((d) -> x(d.index))
         .y((d) -> y(d.value))
         .clipExtent([[-margin.left, -margin.top], [width + margin.right, height + margin.bottom]]);

      #formatPercent = d3.format(".0")

      x = d3.scale.linear()
         .range([0, width])
         #.range([0, width])

      y = d3.scale.linear()
         .range([height, 0])

      line = d3.svg.line()
         .x((d) ->  x(d.index))
         .y((d) ->  y(d.value))
         .interpolate("basis")

      #x.domain(@get('data').map((d) -> d.label))
      #y.domain([0, 12])
      x.domain([0, 100])
      y.domain [d3.min(mins), d3.max(maxs)]

      console.log d3.min(mins)
      console.log d3.max(maxs)
   
      xAxis = d3.svg.axis()
         .scale(x)
         .ticks(20)
         .orient("bottom")

      yAxis = d3.svg.axis()
         .scale(y)
         .orient("left")
         #.tickFormat(formatPercent)

      svg.append("g")
         .attr("class", "x axis")
         .attr("transform", "translate(0," + height + ")")
         .call(xAxis)
       #.selectAll("text")
         #.style("text-anchor", "end")
         #.attr("dx", "-0.8em")
         #.attr("dy", "0.15em")
         #.attr("transform", "rotate(-65)")

      svg.append("g")
         .attr("class", "y axis")
         .call(yAxis)
       .append("text")
         .attr("transform", "rotate(-90)")
         .attr("y", 6)
         .attr("dy", ".71em")
         .style("text-anchor", "end")
         .text("value")

      focus = svg.append("g")
         .attr("transform", "translate(-100,-100)")
         .attr("class", "focus")

      focus.append("circle")
         .attr("r", 3.5)

      focus.append("text")
         .attr("y", -10)


      #svg.selectAll("path")
            #.data(voronoi(@get 'data'))
         #.enter().append("path")
            #.attr("class", "cell")
            #.attr("d", (d) -> "M#{d.join("L")}Z")
            #.style("fill",   "none")
            #.style("stroke", "blue")
            #.style("opacity", "0.2")

      #for i in [0...8]
         #svg.append("path")
            #.attr("class", "line")
            #.attr("d", line(lines[i]))
            #.style("stroke", (d,j) -> color(i))

      svg.append("g")
            .attr("class", "lines")
         .selectAll("path")
            .data(lines)
         .enter().append("path")
            .attr("class", "line")
            .attr("d", (d) -> d.line = this; console.log d; line(d.values))
            .style("stroke", (d,i) -> color(i))

      mouseover = (d) ->
         d3.select(d.line.line).classed("line--hover", true)
         #d.line.line.parentNode.appendChild(d.line.line)
         focus.attr("transform", "translate(" + x(d.index) + "," + y(d.value) + ")")
         focus.select("text").text(d.line.label)

      mouseout = (d) ->
         d3.select(d.line.line).classed("line--hover", false)
         focus.attr("transform", "translate(-100,-100)")

      @voronoiGroup = svg.append("g")
         .attr("class", "voronoi")


      nest = d3.nest()
         .key((d) -> x(d.label) + "," + y(d.value))
         .rollup((v) -> v[0])
         .entries(d3.merge(lines.map((d) -> d.values)))
         .map((d) -> d.values)

      console.log "nest"
      console.log nest

      @voronoiGroup.selectAll("path")
         .data(voronoi(nest))
         .enter().append("path")
            .attr("d", (d) -> "M" + d.join("L") + "Z")
            .datum((d) -> d.point)
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)

      @voronoiGroup.classed("voronoi--show", false)

`export default ChartsLineChartVoronoiComponent`
