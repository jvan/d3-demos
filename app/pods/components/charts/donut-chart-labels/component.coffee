`import Ember from 'ember'`

midAngle = (d) ->
   d.startAngle + (d.endAngle - d.startAngle)/2

ChartsDonutChartLabelsComponent = Ember.Component.extend
   attributeBindings: 'width height data'.w()

   didInsertElement: ->
      width  = @get 'width'
      height = @get 'height'
      radius = Math.min(width, height) / 2

      elementId = @get 'elementId'

      svg = d3.select("#chart")
         .attr("width", width)
         .attr("height", height)
         .append("g")
         .attr("transform", "translate(#{width/2}, #{height/2})")

      svg.append("g")
         .attr("class", "slices")
      svg.append("g")
         .attr("class", "labels")
      svg.append("g")
         .attr("class", "lines")

      pie = d3.layout.pie()
         .sort(null)
         .value((d) -> d.value)

      arc = d3.svg.arc()
         .innerRadius(radius * 0.4)
         .outerRadius(radius * 0.8)

      outerArc = d3.svg.arc()
         .innerRadius(radius * 0.9)
         .outerRadius(radius * 0.9)

      color = d3.scale.category20b()

      svg.select(".slices").selectAll("path")
            .data(pie(@get 'data'))
         .enter().insert("path")
            .style("fill", (d, i) -> color(i))
            .attr("d", (d) -> arc(d))

      svg.select(".labels").selectAll("text")
            .data(pie(@get 'data'))
         .enter().append("text")
            .text((d) -> d.data.label)
            .attr("dy", ".35em")
            .attr("transform", (d) ->
               pos = outerArc.centroid d
               pos[0] = radius * (if midAngle(d) < Math.PI then 1 else -1)
               "translate(#{pos})"
            )
            .attr("text-anchor", (d) ->
               if midAngle(d) < Math.PI then "start" else "end"
            )

      svg.select(".lines").selectAll("polyline")
            .data(pie(@get 'data'))
         .enter().append("polyline")
            .attr("points", (d) ->
               pos = outerArc.centroid d
               pos[0] = radius * 0.95 * (if midAngle(d) < Math.PI then 1 else -1)
               [arc.centroid(d), outerArc.centroid(d), pos]
            )

`export default ChartsDonutChartLabelsComponent`
