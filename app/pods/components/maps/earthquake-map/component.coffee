`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

EarthquakeMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 800
      height = 800

      # Create a mercator map projection.
      projection = d3.geo.mercator()
         .scale((width + 1)/2/Math.PI)
         .translate([width/2, height/2])
         .precision(.1)

      # Create a path generator using the above projection.
      path = d3.geo.path()
         .projection(projection)

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Create a scaling function for the earthquake data.
      radius = d3.scale.sqrt()
         .domain([4.0, 8.0])
         .range([0, 15])

      # Called after the data has been loaded.
      ready = (error, world, earthquakes) ->

         # Draw the land masses.
         svg.append("path")
            .datum(topojson.feature(world, world.objects.land))
            .attr("class", "land")
            .attr("d", path)

         # Draw the country borders.
         svg.append("path")
            .datum(topojson.mesh(world, world.objects.countries), (a, b) ->
               a != b
            )
            .attr("class", "boundary")
            .attr("d", path)

         # Draw the earthquakes using circles. The radius of the circle is
         # proportional to the magnitude of the earthquake.
         svg.selectAll("circle")
               .data(earthquakes.features)
            .enter().append("circle")
               .attr("class", "quake")
               .attr("cx", (d) ->
                  [lon, lat] = [d.geometry.coordinates[0], d.geometry.coordinates[1]]
                  projection([lon, lat])[0]
               )
               .attr("cy", (d) ->
                  [lon, lat] = [d.geometry.coordinates[0], d.geometry.coordinates[1]]
                  projection([lon, lat])[1]
               )
               .attr("r",  (d) -> radius(d.properties.mag))

      # Load the data sources and then render the map.
      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/world-50m.json")
         .defer(d3.json, "#{ENV.baseURL}/data/4.5_month.geojson")
         .await(ready)


`export default EarthquakeMapComponent`
