`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

BasicMultiMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Load data for us states and render the map.
      d3.json "#{ENV.baseURL}/data/us.json", (us) ->

         # Add path elements to the root <svg> element. In this example we use the
         # `data` function. Unlike the previous example (basic) where `dataum` was
         # used, this will result in multiple <path> elements being created (one for
         # each state). This behavior is highlighted by adding a css rule for the odd
         # child elements.
         svg.selectAll("path")
              .data(topojson.feature(us, us.objects.states).features)
           .enter().append("path")
              .attr("class", "states")
              .attr("d", path)

`export default BasicMultiMapComponent`
