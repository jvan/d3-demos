`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

BasicMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Load data for us states and render the map.
      d3.json "#{ENV.baseURL}/data/us.json", (us) ->

         # Add a path element to the root <svg> element. In this example we use the
         # `datum` function which does not compute a join. This can be useful when
         # the data is not expected to change.
         svg.append("path")
           .datum(topojson.feature(us, us.objects.states))
           .attr("class", "states")
           .attr("d", path)

`export default BasicMapComponent`
