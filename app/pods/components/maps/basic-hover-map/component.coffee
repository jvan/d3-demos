`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

BasicHoverMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Load data for us states and render the map.
      d3.json "#{ENV.baseURL}/data/us.json", (us) ->

         # Use `mouseover` and `mouseout` events to change the opacity of the
         # <path> element when hovering.
         svg.selectAll("path")
              .data(topojson.feature(us, us.objects.states).features)
           .enter().append("path")
              .attr("class", "states")
              .attr("d", path)
              .style("opacity", 0.5)
              .on("mouseover", ->
                 d3.select(this).style("opacity", 1))
              .on("mouseout", ->
                 d3.select(this).style("opacity", 0.5))

`export default BasicHoverMapComponent`
