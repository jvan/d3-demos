`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

BasicHoverMapComponent = Ember.Component.extend

   hoverState: null

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      stateNames = d3.map()

      # Load data for us states and render the map.
      ready = (error, us, states) =>

         console.log stateNames

         _this = this

         # Use `mouseover` and `mouseout` events to change the opacity of the
         # <path> element when hovering.
         svg.selectAll("path")
              .data(topojson.feature(us, us.objects.states).features)
           .enter().append("path")
              .attr("class", "states")
              .attr("id", (d) -> "state-#{d.id}")
              .attr("d", path)
              .style("opacity", 0.5)
              .on("mouseover", (d) ->
                 d3.select(this).style("opacity", 1)
                 _this.set 'hoverState', stateNames.get(d.id)
              )
              .on("mouseout", (d) ->
                 d3.select(this).style("opacity", 0.5)
                 _this.set 'hoverState', null
              )

      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/us.json")
         .defer(d3.csv,  "#{ENV.baseURL}/data/us-states.csv",
            (d) -> stateNames.set(d.id, d.name))
         .await(ready)


`export default BasicHoverMapComponent`
