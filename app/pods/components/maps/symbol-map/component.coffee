`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

SymbolMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Create a scaling function for population data.
      radius = d3.scale.sqrt()
         .domain([0, 1e6])
         .range([0, 10])

      # Called after the data has been loaded.
      ready = (error, us, centroid) ->

         # Draw the map. Since we do not need to interact with the individual
         # states we can use the `datum` function and create a single <path>
         # element.
         svg.append("path")
            .datum(topojson.feature(us, us.objects.states))
            .attr("class", "states")
            .attr("d", path)

         # Render the population data. For each state draw a circle whose
         # radius is proportional to the population of the state.
         svg.selectAll(".symbol")
               # Sort the data so larger circles are drawn first.
               .data(centroid.features.sort((a,b) -> b.properties.population - a.properties.population))
            .enter().append("path")
               .attr("class", "symbol")
               .attr("d", path.pointRadius((d) -> radius(d.properties.population)))

      # Load the data sources and then render the map. The data objects will be
      # passed to the `ready` function once all the data is finished loading.
      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/us.json")
         .defer(d3.json, "#{ENV.baseURL}/data/us-state-centroids.json")
         .await(ready)

`export default SymbolMapComponent`
