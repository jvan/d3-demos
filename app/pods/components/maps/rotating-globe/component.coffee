`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

RotatingGlobeComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 720

      speed = 1e-2
      start = Date.now()

      sphere = {type: "Sphere"}

      projection = d3.geo.orthographic()
         .scale(height/2.1)
         .translate([width/2, height/2])
         .clipAngle(90)
         .precision(.5)

      graticule = d3.geo.graticule()

      canvas = d3.select("#map").append("canvas")
         .attr("width", width)
         .attr("height", height)

      context = canvas.node().getContext("2d")

      path = d3.geo.path()
         .projection(projection)
         .context(context)

      d3.json "#{ENV.baseURL}/data/world-110m.json", (error, topo) ->
         land = topojson.feature(topo, topo.objects.land)
         borders = topojson.mesh(topo, topo.objects.countries, (a, b) -> a != b)
         grid = graticule()

         d3.timer ->
            projection.rotate([speed * (Date.now() - start), -15])

            context.clearRect(0, 0, width, height)

            context.beginPath()
            path(sphere)
            context.lineWidth = 3
            context.strokeStyle = "#000"
            context.stroke()

            context.beginPath()
            path(sphere)
            context.fillStyle = "#fff"
            context.fill()

            context.beginPath()
            path(land)
            context.fillStyle = "#222"
            context.fill()

            context.beginPath()
            path(borders)
            context.lineWidth = .5
            context.strokeStyle = "#fff"
            context.stroke()

            context.beginPath()
            path(grid)
            context.lineWidth = .5
            context.strokeStyle = "rgba(119,119,119,.5)"
            context.stroke()

      d3.select(self.frameElement).style("height", "#{height}px")

`export default RotatingGlobeComponent`
