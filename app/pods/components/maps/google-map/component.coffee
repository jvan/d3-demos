`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

GoogleMapComponent = Ember.Component.extend

   didInsertElement: ->
      # Crate a new google map object.
      map = new google.maps.Map(d3.select("#map").node(),
         zoom: 8
         center: new google.maps.LatLng(37.76487, -122.41948)
         mapTypeId: google.maps.MapTypeId.TERRAIN
         disableDefaultUI: true
      )

      # Load the station data and create an overlay for the map.
      d3.json "#{ENV.baseURL}/data/stations.json", (data) ->
         overlay = new google.maps.OverlayView()

         overlay.onAdd = ->
            layer = d3.select(this.getPanes().overlayLayer).append("div")
               .attr("class", "stations")

            overlay.draw = ->
               projection = this.getProjection()
               padding = 10

               path = d3.geo.path()
                  .projection(projection)

               transform = (d) ->
                  d = new google.maps.LatLng(d.value[1], d.value[0])
                  d = projection.fromLatLngToDivPixel(d)

                  d3.select(this)
                     .style("left", "#{d.x - padding}px")
                     .style("top",  "#{d.y - padding}px")

               marker = layer.selectAll("svg")
                     .data(d3.entries(data))
                     .each(transform) # update existing markers
                  .enter().append("svg")
                     .each(transform)
                     .attr("class", "marker")

               marker.append("circle")
                  .attr("r", 4.5)
                  .attr("cx", padding)
                  .attr("cy", padding)
                  .attr("d", path)

               marker.append("text")
                  .attr("x", padding + 7)
                  .attr("y", padding)
                  .attr("dy", ".31em")
                  .text((d) -> d.key)
                  .attr("d", path)

         overlay.setMap map

`export default GoogleMapComponent`
