`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

MercatorMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 800
      height = 600

      # Create a mercator map projection.
      projection = d3.geo.mercator()
         .scale((width + 1)/2/Math.PI)
         .translate([width/2, height/2])
         .precision(.1)

      # Create a path generator using the above projection.
      path = d3.geo.path()
         .projection(projection)

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Load data for us states and render the map.
      d3.json "#{ENV.baseURL}/data/world-50m.json", (world) ->

         # Draw the land masses.
         svg.append("path")
            .datum(topojson.feature(world, world.objects.land))
            .attr("class", "land")
            .attr("d", path)

         # Draw the country borders.
         svg.append("path")
            # Generate a mesh from the country data.
            .datum(topojson.mesh(world, world.objects.countries), (a, b) ->
               a != b
            )
            .attr("class", "boundary")
            .attr("d", path)

`export default MercatorMapComponent`
