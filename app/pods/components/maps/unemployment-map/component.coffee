`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

UnemploymentMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      unemploymentRate = d3.map()

      # Create a scaling function for the unemployment data.
      quantize = d3.scale.quantize()
          .domain([0, .15])
          .range(d3.range(9).map((i) -> "q#{i}-9" ))

      # Called after the data has been loaded.
      ready = (error, us) ->

         # Draw the counties, using the unemployment data to color the <path>
         # elements. 
         svg.append("g") # create a group element to store the county paths.
            .selectAll("path")
              .data(topojson.feature(us, us.objects.counties).features)
            .enter().append("path")
              .attr("class", (d) -> quantize(unemploymentRate.get(d.id)))
              .attr("d", path)

         # Draw the state borders. Since we do not need to interact with the
         # individual states we can use the `datum` function and create a
         # single <path> element.
         svg.append("path")
            .datum(topojson.feature(us, us.objects.states))
            .attr("class", "states")
            .attr("d", path)

      # Load the data sources and then render the map. The unempolyment data
      # will be processed and stored by id.
      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/us.json")
         .defer(d3.csv,  "#{ENV.baseURL}/data/unemployment.csv",
            (d) -> unemploymentRate.set(d.id, +d.rate))
         .await(ready)

`export default UnemploymentMapComponent`
