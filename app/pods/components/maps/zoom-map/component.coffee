`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

ZoomMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 897
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)
         .on("click", stopped, true)

      active = d3.select(null)

      zoomed = ->
         svg.style("stroke-width", "#{1.5 / d3.event.scale}px")
         svg.attr("transform", "translate(#{d3.event.translate})scale(#{d3.event.scale})")

      zoom = d3.behavior.zoom()
         .translate([0, 0])
         .scale(1)
         .scaleExtent([1, 8])
         .on("zoom", zoomed)

      clicked = (d) ->
         if active.node() == this
            return reset()

         active.classed("active", false)
         active = d3.select(this).classed("active", true)

         bounds = path.bounds(d)
         dx = bounds[1][0] - bounds[0][0]
         dy = bounds[1][1] - bounds[0][1]
         x = (bounds[0][0] + bounds[1][0]) / 2
         y = (bounds[0][1] + bounds[1][1]) / 2
         scale = .9 / Math.max(dx / width, dy / height)
         translate = [width / 2 - scale * x, height / 2 - scale * y]

         svg.transition()
            .duration(750)
            .call(zoom.translate(translate).scale(scale).event)

      reset = ->
        active.classed("active", false)
        active = d3.select(null)

        svg.transition()
            .duration(750)
            .call(zoom.translate([0, 0]).scale(1).event)

      stopped = ->
        if d3.event.defaultPrevented
           d3.event.stopPropagation()

      ready = (error, us) ->

        svg.selectAll("path")
           .data(topojson.feature(us, us.objects.states).features)
           .enter().append("path")
           .attr("d", path)
           .attr("class", "feature")
           .on("click", clicked)

        svg.append("path")
           .datum(topojson.mesh(us, us.objects.states, (a, b) -> a != b))
           .attr("class", "mesh")
           .attr("d", path)

      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/us.json")
         .await(ready)

`export default ZoomMapComponent`
