`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

ZipcodeMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 894
      height = 500

      # Create a map projection. Since we are using a <canvas> element instead
      # of an <svg> we do not need a path generator.
      projection = d3.geo.albersUsa()
         .scale(1000)
         .translate([width/2, height/2])

      # Create a color map for the different zip code areas.
      colors = d3.scale.category10()

      # Create the <canvas> element for the map.
      canvas = d3.select("#map").insert("canvas")
         .attr("width", width)
         .attr("height", height)
         .style("opacity", 1)

      # Load the zip code data and render the map.
      d3.tsv "#{ENV.baseURL}/data/zipcodes.tsv", (zipcodes) ->

         # Initalize arrays for storing the zip codes by first digit.
         zips = ([] for i in [0...10])

         # Computet the projection for each zip code and store in the `zips`
         # array.
         zipcodes.forEach (d) ->
            p = projection([+d.lon, +d.lat])
            if p?
               # Add the projected point to the appropriate array.
               zips[d.zip[0]].push
                  x: Math.round(p[0])
                  y: Math.round(p[1])

         # Draw the zip codes as points. 
         context = canvas.node().getContext("2d")

         for zip of zips
            # Set a different color for each zone (defined by first digit of
            # the zip code).
            context.fillStyle = colors(zip)

            # Draw each zip code as a single pixel.
            zips[zip].forEach (d) ->
               context.fillRect(d.x, d.y, 1, 1)

`export default ZipcodeMapComponent`
