`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

BasicSelectMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Load data for us states and render the map.
      d3.json "#{ENV.baseURL}/data/us.json", (us) ->

         # Use `click` event to select <path> elements.
         svg.selectAll("path")
               .data(topojson.feature(us, us.objects.states).features)
            .enter().append("path")
               .attr("class", "states")
               .attr("d", path)
               .style("opacity", 0.5)
               .on("mouseover", ->
                  d3.select(this).style("opacity", 1))
               .on("mouseout", ->
                  d3.select(this).style("opacity", 0.5))
               .on("click", ->
                  # Get the current selected state of the path element.
                  selected = d3.select(this).classed("selected")
                  # Toggle the selected state.
                  d3.select(this).classed("selected", !selected))

`export default BasicSelectMapComponent`
