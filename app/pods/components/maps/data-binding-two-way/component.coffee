`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

BasicSelectMapComponent = Ember.Component.extend

   allStates: Em.A()

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      # Load data for us states and render the map.
      ready = (error, us, states) =>

         _this = this

         # Use `click` event to select <path> elements.
         svg.selectAll("path")
               .data(topojson.feature(us, us.objects.states).features)
            .enter().append("path")
               .attr("class", "states")
               .attr("d", path)
               .attr("id", (d) -> "state-#{d.id}")
               .style("opacity", 0.5)
               .on("mouseover", ->
                  d3.select(this).style("opacity", 1))
               .on("mouseout", ->
                  d3.select(this).style("opacity", 0.5))
               .on("click", (d) ->
                  # Get the current selected state of the path element.
                  selected = d3.select(this).classed("selected")

                  state = _this.get('allStates').findBy 'id', "#{d.id}"
                  if not selected
                     Em.set state, 'selected', true
                  else
                     Em.set state, 'selected', false

                  # Toggle the selected state.
                  d3.select(this).classed("selected", !selected))

      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/us.json")
         .defer(d3.csv,  "#{ENV.baseURL}/data/us-states.csv",
            (d) => @get('allStates').pushObject
                     id: d.id
                     name: d.name
                     selected: false
         )
         .await(ready)

   actions:

      toggleState: (state) ->
         selected = Em.get state, 'selected'
         Em.set state, 'selected', !selected

         path = d3.select "#state-#{state.id}"
         selected = path.classed("selected")
         path.classed("selected", !selected)

`export default BasicSelectMapComponent`
