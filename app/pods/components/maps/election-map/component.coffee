`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

ElectionMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      electionResults = d3.map()

      # Called after the data has been loaded.
      ready = (error, us) ->

         svg.append("g").attr("class", "states")
            .selectAll("path")
               .data(topojson.feature(us, us.objects.states).features)
            .enter().append("path")
               # Add an additional class based on the winner of the state.
               .attr("class", (d) ->
                  winner = electionResults.get(d.id)
                  if winner == "Obama"
                     return "blue"
                  else if winner == "McCain"
                     return "red"
               )
               .attr("d", path)

      # Load the data sources and then render the map. The election data
      # will be processed and stored by id.
      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/us.json")
         .defer(d3.csv,  "#{ENV.baseURL}/data/election-2008.csv",
            (d) -> electionResults.set(d.id, d.winner))
         .await(ready)

`export default ElectionMapComponent`
