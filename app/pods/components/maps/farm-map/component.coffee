`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

FarmMapComponent = Ember.Component.extend

   didInsertElement: ->
      width  = 960
      height = 500

      # Create a new path generator. The default projection is albersUsa.
      path = d3.geo.path()

      # Create the <svg> element for the the map.
      svg = d3.select("#map").append("svg")
         .attr("width", width)
         .attr("height", height)

      div = d3.select("#map").append("div")
         .attr("class", "tooltip")
         .style("opacity", 0)

      farmRate = d3.map()
      farmName = d3.map()

      color_domain = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000]
      ext_color_domain = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000]

      legend_labels = ["< 500", "500+", "1000+", "1500+", "2000+", "2500+", "3000+", "3500+", "4000+", "4500+", "5000+", "5500+", "6000+"]

      color = d3.scale.threshold()
         .domain(color_domain)
         .range(["#dcdcdc", "#d0d6cd", "#bdc9be", "#aabdaf", "#97b0a0", "#84a491", "#719782", "#5e8b73", "#4b7e64", "#387255", "#256546", "#125937", "#004d28"])


      # Called after the data has been loaded.
      ready = (error, us) ->

         # Draw the counties, using the unemployment data to color the <path>
         # elements. 
         svg.append("g") # create a group element to store the county paths.
            .selectAll("path")
              .data(topojson.feature(us, us.objects.counties).features)
            .enter().append("path")
              .attr("d", path)
              .style("fill", (d) ->
                  r = farmRate.get(d.id)
                  if r?
                     color(farmRate.get(d.id))
                  )
              .style("opacity", 0.8)
              .on("mouseover", (d) ->
                 d3.select(this).transition().duration(300).style("opacity", 1)
                 div.transition().duration(300)
                    .style("opacity", 1)
                 div.text("#{farmName.get(d.id)}:#{farmRate.get(d.id)}")
                    .style("left", "#{d3.event.pageX}px")
                    .style("top",  "#{d3.event.pageY - 30}px")
              )
              .on("mouseout", ->
                 d3.select(this)
                    .transition().duration(300)
                    .style("opacity", 0.8)
                 div.transition().duration(300)
                    .style("opacity", 0)
              )

      # Load the data sources and then render the map. The unempolyment data
      # will be processed and stored by id.
      queue()
         .defer(d3.json, "#{ENV.baseURL}/data/us.json")
         .defer(d3.csv,  "#{ENV.baseURL}/data/farms.csv",
            (d) ->
               farmRate.set(d.id, +d.rate)
               farmName.set(d.id,  d.name))
         .await(ready)

`export default FarmMapComponent`
