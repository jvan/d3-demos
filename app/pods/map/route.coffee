`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

MapRoute = Ember.Route.extend

   mapComponent: Em.computed 'title', ->
      "maps/#{@get 'title'}"

   model: (params) ->
      @set 'title', params.map_name

   setupController: (controller, model) ->
      @_super arguments...

      controller.set 'model.title', model.get('title')

      url = 'https://bitbucket.org/jvan/d3-demos/raw/HEAD/app/pods/components/maps'

      getCoffee = $.ajax
         url: "#{url}/#{@get 'title'}/component.coffee"
         dataType: 'text'

      getCss = $.ajax
         url: "#{url}/#{@get 'title'}/styles.css"
         dataType: 'text'

      promises =
        coffee: getCoffee
        css:    getCss

      Ember.RSVP.hash(promises).then (hash) ->
         controller.set 'model.sourceCode',
          coffee: hash.coffee
          css:    hash.css

`export default MapRoute`
