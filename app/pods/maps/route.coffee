`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

MapsRoute = Ember.Route.extend

   model: ->
      @store.find 'demo', { category: 'map' }

`export default MapsRoute`
