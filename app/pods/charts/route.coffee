`import Ember from 'ember'`
`import ENV from 'd3-demos/config/environment'`

ChartsRoute = Ember.Route.extend

   model: ->
      @store.find 'demo', { category: 'chart' }

`export default ChartsRoute`
