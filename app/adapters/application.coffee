`import DS from 'ember-data'`

ApplicationAdapter = DS.FixtureAdapter.extend
  queryFixtures: (fixtures, query, type) ->
    key = Ember.keys(query)[0]
    fixtures.filterBy key, query[key]



`export default ApplicationAdapter`
