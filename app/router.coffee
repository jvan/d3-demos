`import Ember from 'ember'`
`import config from './config/environment'`

Router = Ember.Router.extend
  location: config.locationType

Router.map ->

   @route 'charts'
   @route 'maps'

   @route 'chart', path: '/charts/:chart_name'
   @route 'map',   path: '/maps/:map_name'

`export default Router`
