`import Ember from 'ember'`

# This function receives the params `params, hash`
highlightSyntax = (params) ->
  if params[0]?
    block = params[0]
    highlight = hljs.highlightAuto(block)
    Ember.String.htmlSafe highlight.value


HighlightSyntaxHelper = Ember.HTMLBars.makeBoundHelper highlightSyntax

`export { highlightSyntax }`

`export default HighlightSyntaxHelper`
