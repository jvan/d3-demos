`import DS from 'ember-data'`

Demo = DS.Model.extend
  name:     DS.attr 'string'
  category: DS.attr 'string'

Demo.reopenClass
  FIXTURES: [
    { id: 1, category: "map", name: "basic-map" },
    { id: 2, category: "map", name: "basic-multi-map" },
    { id: 3, category: "map", name: "basic-hover-map" },
    { id: 4, category: "map", name: "basic-select-map" },
    { id: 5, category: "map", name: "data-binding-one-way" },
    { id: 6, category: "map", name: "data-binding-two-way" },
    { id: 7, category: "map", name: "earthquake-map" },
    { id: 8, category: "map", name: "election-map" },
    { id: 9, category: "map", name: "farm-map" },
    { id: 10, category: "map", name: "google-map" },
    { id: 11, category: "map", name: "mercator-map" },
    { id: 12, category: "map", name: "rotating-globe" },
    { id: 13, category: "map", name: "symbol-map" },
    { id: 14, category: "map", name: "unemployment-map" },
    { id: 15, category: "map", name: "zipcode-map" },
    { id: 16, category: "map", name: "zoom-map" },
    { id: 17, category: "chart", name: "bar-chart" },
    { id: 18, category: "chart", name: "bar-chart-sortable" },
    { id: 19, category: "chart", name: "donut-chart" },
    { id: 20, category: "chart", name: "donut-chart-labels" },
    { id: 21, category: "chart", name: "donut-chart-animated" },
    { id: 22, category: "chart", name: "line-chart" },
    { id: 23, category: "chart", name: "line-chart-interpolated" },
    { id: 24, category: "chart", name: "line-chart-multi" },
    { id: 25, category: "chart", name: "line-chart-voronoi" },
    { id: 26, category: "chart", name: "voronoi-tesselation" },
    { id: 27, category: "chart", name: "voronoi-topology" }
  ]

`export default Demo`
